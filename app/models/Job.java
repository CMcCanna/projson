package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Job extends Model {

    private static Model.Find<String, Job> find = new Model.Finder<>(Job.class);

    @Id
    public String code;
    public String description;

    public static List<Job> findAll() {
        return Job.find.orderBy("code").findList();
    }

}
