package controllers;

import models.Job;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.info;

import java.util.List;


public class Jobs extends Controller {

    public static final Form<Job> jobForm = Form.form(Job.class);

    public Result list() {
        List<Job> allJobs = Job.findAll();
        return ok(Json.toJson(allJobs));
    }

    public Result newJob() {
        return ok(info.render(jobForm));
    }

    public Result save() {
        //extract data from the form
        Form<Job> ourForm = jobForm.bindFromRequest();
        Job job = ourForm.get();
        //persist to db
        job.save();

        //refresh the view
        return redirect(routes.Jobs.newJob());
    }
}
